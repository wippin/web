
/*
 * Kopatheme
 * Released under the MIT License.
 */
function mainSliderHome(){
    /*_____________ Slider Pro _____________*/

    var psj_1 = jQuery('.slider-pro-1');
    if (psj_1.length) {
        psj_1.each(function(){   
            jQuery(this).sliderPro({
                forceSize: 'fullWidth',
                width: 1366,
                height: 866,
                arrows: true,
                buttons: false,
                waitForLayers: false,
                autoplay: true,
                fadeOutPreviousSlide: true,
                autoScaleLayers: true,
                slideDistance: 0,
                autoplayDelay: 3000,
                init: function(){
                   psj_1.next(".loading").hide();    
                   psj_1.show();  
                   psj_1.find('.sp-previous-arrow').append('<span><strong>prev</strong></span>');
                   psj_1.find('.sp-next-arrow').append('<span><strong>next</strong></span>');
                }
            });
        });
    };


}
 
'use strict';


/**
 * Code
 * -------------------------------------------------------------------
 */


jQuery(document).ready(function() {
    var i = 0; 


/*_____________ 1. Header _____________*/

    /*--- main-menu ---*/

    if(sfmmenu_ul.length){
        sfmmenu_ul.superfish({
            speed: 'fast',
            delay: '300'
        });

        /*--- sub-menu direction ---*/
        
        mmenu_sub_direction(mmenu_ul,mmenu_li,sfmmenu_ul);

    }


    /*--- mobile-menu ---*/   

    if(jQuery('.mobile-nav').length) {
        var sj_mb_nav = jQuery('.mobile-nav');
        sj_mb_nav.each(function(){
            var sj_mbn_pull = jQuery(this).children('span'),
                sj_mbn_ul   = jQuery(this).children('.mobile-menu');

            sj_mbn_pull.on('click',function () {
                sj_mbn_ul.slideToggle('normal').toggleClass('active');
            });
            jQuery('.mobile-menu').navgoco({
                accordion: true
            });
            jQuery('.caret').removeClass('caret');

            jQuery("html").mouseup(function (e){
                if (!sj_mb_nav.is(e.target) && sj_mb_nav.has(e.target).length === 0){
                    sj_mbn_ul.slideUp().removeClass('active');
                }
            });

        });
    }

    //slide-area
    // var h_window   = jQuery(window).height(),
    //     // area_ct    = jQuery('.kopa-area-custom'),
    //     hb_menu    = jQuery('.hamburger-menu'),
    //     hb_area    = jQuery('.slide-area'),
    //     hb_overlay = jQuery('.body-overlay'),
    //     hb_main_ct = jQuery('.main-container'),
    //     hb_close   = hb_area.find('.close-btn');

    // if(hb_area.length) {
    //     hb_area.find('.slide-container').height(h_window).mCustomScrollbar();
    // }
    // if(area_ct.length) {
    //     area_ct.css('min-height',h_window);
    // }

    // if(hb_menu.length) {
    //     hb_menu.on('click',function(event) {
    //         event.preventDefault();
    //         hb_area.toggleClass('active');
    //         hb_overlay.toggleClass('active');
    //         hb_main_ct.toggleClass('scale-down');
    //     });
    // }
    // hb_close.on('click',function(event) {
    //     event.preventDefault();
    //     hb_area.removeClass('active');
    //     hb_overlay.removeClass('active');
    //     hb_main_ct.removeClass('scale-down');
    // });
    // hb_overlay.on('click',function(event) {
    //     hb_close.trigger('click');
    // });
 
    /*--- slide-menu ---*/
    // if(jQuery('.slide-menu').length) {
    //     jQuery('.slide-menu').navgoco({
    //         accordion: true
    //     });
    //     jQuery('.caret').removeClass('caret');
    // }
    // /*--- slide-mobile-menu ---*/
    // if(jQuery('.slide-mobile-menu').length) {
    //     jQuery('.slide-mobile-menu').navgoco({
    //         accordion: true
    //     });
    //     jQuery('.caret').removeClass('caret');
    // }


    /*--- search-box ---*/
    var s_search = jQuery('.kopa-search-box');
    if(s_search.length) {
        var s_search_label = jQuery(this).find('.search-label');
        s_search_label.on('click',function() {
            s_search.find('.search-text').focus();
            s_search_label.toggleClass('active');
        });
        jQuery("html").mouseup(function (e){
            if (!s_search.is(e.target) && s_search.has(e.target).length === 0){
                s_search_label.removeClass('active');
                s_search.find('.search-text').blur();
            }
        });
    }

    //search-area
    // var s_area  = jQuery('.search-area'),
    //     s_btn   = jQuery('.kopa-search'),
    //     s_close = s_area.find('.close-btn'),
    //     s_form  = s_area.find('.search-form-area');

    // if(s_btn.length) {
    //     s_btn.on('click',function(event) {
    //         event.preventDefault();
    //         s_area.toggleClass('active');
    //         window.setTimeout((function() {
    //             s_form.find('.search-text').focus();
    //         }),100);
            
    //     });
    // }
    // s_close.on('click',function(event) {
    //     event.preventDefault();
    //     s_area.removeClass('active');
    // });
    // jQuery("html").mouseup(function (e){
    //     if (!s_form.is(e.target) && s_form.has(e.target).length === 0){
    //         s_close.trigger('click');
    //     }
    // });

    // /*--- ESC key ---*/
    // jQuery(document).keyup(function(e) {
    //      if (e.keyCode == 27) { 
    //         hb_close.trigger('click');
    //         s_close.trigger('click');
    //     }
    // });


/*_____________ Scroll top Top _____________*/   


    // jQuery(window).on('scroll',function(){
    //     if (jQuery(this).scrollTop() > 200) {
    //         jQuery('.scrollup').fadeIn();
    //     } else {
    //         jQuery('.scrollup').fadeOut();
    //     }
    // }); 
    // jQuery('.scrollup').on('click',function(){
    //     jQuery("html, body").animate({ scrollTop: 0 }, 600);
    //     return false;
    // });


/*_____________ Owl Carousel _____________*/

    var owl1 = jQuery(".owl-carousel-1");
    if(owl1.length){
        owl1.each(function(){
            var o1_pag = jQuery(this).data('pagination'),
                o1_nav = jQuery(this).data('navigation'),
                o1_item = jQuery(this).data('item'),
                o1_item_dt = jQuery(this).data('item-desktop'),
                o1_item_tl = jQuery(this).data('item-tablet'),
                o1_item_tls = jQuery(this).data('item-tablet-small');

            jQuery(this).owlCarousel({
                items: o1_item,
                itemsDesktop: o1_item_dt,
                itemsTablet: o1_item_tl,
                itemsTabletSmall: o1_item_tls,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: false,
                pagination: o1_pag,
                navigation: o1_nav,
                addClassActive: true,
                afterAction: function(){
                    jQuery(".faded").removeClass("faded");
                    owl1.find(".active").first().addClass("faded");
                    owl1.find(".active").last().addClass("faded");
                },
                afterInit: function(){
                    owl1.next(".loading").hide();    
                }
            });
        });
    }

    var owl2 = jQuery(".owl-carousel-2");
    if(owl2.length){
        owl2.each(function(){
            var o2_pag = jQuery(this).data('pagination'),
                o2_nav = jQuery(this).data('navigation'),
                o2_item = jQuery(this).data('item'),
                o2_item_dt = jQuery(this).data('item-desktop'),
                o2_item_tl = jQuery(this).data('item-tablet'),
                o2_item_tls = jQuery(this).data('item-tablet-small');

            jQuery(this).owlCarousel({
                items: o2_item,
                itemsDesktop: o2_item_dt,
                itemsTablet: o2_item_tl,
                itemsTabletSmall: o2_item_tls,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: true,
                pagination: o2_pag,
                navigation: o2_nav,
                addClassActive: true
            });
        });
    }

    var owl3 = jQuery(".owl-carousel-3");
    if(owl3.length){
        owl3.each(function(){
            var o3_pag = jQuery(this).data('pagination'),
                o3_nav = jQuery(this).data('navigation');

            jQuery(this).owlCarousel({
                singleItem: true,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: true,
                pagination: o3_pag,
                navigation: o3_nav,
                addClassActive: true
            });
        });
    }

    var owl4 = jQuery(".owl-carousel-4");
    if(owl4.length){
        owl4.each(function(){
            var o4_pag = jQuery(this).data('pagination'),
                o4_nav = jQuery(this).data('navigation');

            jQuery(this).owlCarousel({
                singleItem: true,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: true,
                pagination: o4_pag,
                navigation: o4_nav,
                addClassActive: true
            });
        });
    }

    var owl5 = jQuery(".owl-carousel-5");
    if(owl5.length){
        owl5.each(function(){
            var o5_pag = jQuery(this).data('pagination'),
                o5_nav = jQuery(this).data('navigation');

            jQuery(this).owlCarousel({
                singleItem: true,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: true,
                pagination: o5_pag,
                navigation: o5_nav,
                addClassActive: true
            });
        });
    }



/*_____________ Popup _____________*/

    function popup(){
        var sj_pu = jQuery('.sj-popup');
        if (sj_pu.length){
            var sj_pu_vd  = sj_pu.find('.popup-vd-link'),
                sj_pu_img = sj_pu.find('.popup-img-link');
            sj_pu_vd.each(function(){
                jQuery(this).magnificPopup({
                    iframe: {
                        patterns: {
                            vimeo: {
                                index: 'vimeo.com/',
                                id: '/',
                                src: 'http://player.vimeo.com/video/%id%?autoplay=1'
                            },
                        }
                    },
                    type: 'iframe'
                }); 
            });    
            sj_pu_img.each(function(){
                jQuery(this).magnificPopup({
                    type: 'image',
                    tLoading: 'Loading image #%curr%...',
                    mainClass: 'mfp-img-mobile',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,1]
                    },
                    image: {
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                    }
                }); 
            });    
        }      
    }
    popup();

/*_____________ Fitvid _____________*/

    if (jQuery(".video-wrapper").length > 0) {
        jQuery(".video-wrapper").fitVids();
    };


/*_____________ Masonry _____________*/

    var masonry_01 = jQuery('.kopa-entry-list');

    if (jQuery(masonry_01).length) {

        jQuery(masonry_01).each(function(){

            var masonry_content_01 = jQuery(this).children('.row');
            masonry_content_01.imagesLoaded(function(){
                masonry_content_01.masonry({
                    columnWidth: 1,
                    itemSelector : '.ms-item-01.show'
                });

            });

        });
    }


/*_____________ Filter _____________*/

    var filter_01 = jQuery('.module-filter-1');

    if (jQuery(filter_01).length) {

        jQuery(filter_01).each(function(){

            var filter_content_01 = jQuery(this).find('.masonry-container'),
                filter_nav_01 = jQuery(this).find('.masonry-filter').children('a');
            var masonryOptions = {
                columnWidth: 1,
                itemSelector : '.ms-item-02.show'
            };
            filter_content_01.imagesLoaded(function(){

                var m_grid = filter_content_01.masonry( masonryOptions);

                filter_nav_01.on('click',function(event){
                    event.preventDefault();
                    filter_nav_01.removeClass('active');
                    jQuery(this).addClass('active');
                    var m_filter_val = jQuery(this).data('val');

                    filter_content_01.find('.ms-item-02').each(function(){
                        var m_item_val = jQuery(this).data('val').toString().split(',');                
                        var a = m_item_val.indexOf(m_filter_val.toString()) > -1;

                        if (m_filter_val == "*") {
                            jQuery(this).removeClass('hide');
                            jQuery(this).addClass('show');
                        } else {
                            if ( a == true) {
                                jQuery(this).removeClass('hide');
                                jQuery(this).addClass('show');  
                            } else {
                                jQuery(this).removeClass('show');
                                jQuery(this).addClass('hide');
                            }
                        }                               
                    });
                    filter_content_01.masonry('layout');

                });

            });

        });
    }


/*_____________ Custom _____________*/

    

    /*--- area-space ---*/
    sj_area_space();

    /*--- order-list ---*/

    // function sj_order_list(){
    //     var sj_ulod = jQuery('.kopa-order-list');
    //     if(sj_ulod.length){
    //         sj_ulod.each(function(){
    //             var sj_liod = jQuery(this).children();
    //             sj_liod.each(function() {
    //                 var sj_liod_num     = '0' + (jQuery(this).index() + 1);
    //                 var sj_liod_num_1   =  (jQuery(this).index() + 1) + '.';
    //                 jQuery(this).find('.kopa-order-num').html(sj_liod_num);
    //                 jQuery(this).find('.kopa-order-num-1').html(sj_liod_num_1);
    //             });
    //         });
    //     }
    // }
    // sj_order_list();

    /*--- background-item ---*/

    var bg_item = jQuery('.sj-bg-item');
    if(bg_item.length){
        bg_item.each(function(){
            var bg_item_content = jQuery(this).data('bg');
            jQuery(this).css('background-image', 'url("' + bg_item_content  + '")');
        })
    }

    /*--- header ---*/

    var kopa_header = jQuery("[class*='kopa-page-header-']");
    if(kopa_header.length > 1){
        kopa_header.each(function(){
            jQuery(this).css('z-index', 999 - i);
            ++i;
        });
    }

    /*--- countup ---*/

    var sj_counterup = jQuery('.sj-counter');
    if(sj_counterup.length) {
        sj_counterup.each(function(){
            jQuery(this).counterUp({
                delay: 10,
                time: 1000
            });
        });
    }

    


/*_____________ MatchHeight _____________*/

    function mh_1(){
        jQuery('.kopa-mh').children().matchHeight();
    }
    if(jQuery('.kopa-mh').length){
        mh_1();
    }

});


/* sj_area_space */

function sj_area_space(){
    var w_w  = jQuery(window).width(), 
        ct_w = jQuery('.container').width();
           
    if(jQuery('.container').length == 0){
        ct_w = 1170;
    }

    var sp_w = (w_w - ct_w)/2,
        pl_w = jQuery('.sj-plw'),
        pr_w = jQuery('.sj-prw');

    pl_w.css({
        "padding-left": sp_w
    });
    pr_w.css({
        "padding-right": sp_w
    });
}

/* mmenu_sub_direction */

var mmenu_ul   = jQuery('.main-nav').find('.main-menu'),
    mmenu_li   = mmenu_ul.children('li'),
    sfmmenu_ul = jQuery('.main-nav').find('.sf-menu');
function menu_rtl(mli_child, mli_osl, mlic_w){
    mli_child.each(function(){
        var mlic_num  = jQuery(this).parents('li').length - 1,
            mlic_osl  = mli_osl + (mlic_w * mlic_num),
            mlic_osr  = jQuery(window).width() - (mlic_osl);
        if(mlic_w > mlic_osr){
            jQuery(this).addClass('rtl');
        }
    });
}
function mmenu_sub_direction(mmenu_ul,mmenu_li,sfmmenu_ul){
    mmenu_li.each(function(){
        var mli_osl   = jQuery(this).offset().left,
            mli_osr   = jQuery(window).width() - (mli_osl + jQuery(this).outerWidth()),
            mli_child = jQuery(this).find('ul'),
            mlic_w    = mli_child.outerWidth();

        menu_rtl(mli_child, mli_osl, mlic_w);

        var mlic_rtl = jQuery('.rtl');
        mlic_rtl.each(function(){   
            var mlic_rtl_num  = jQuery(this).parents('.rtl').length + 1,
                mlic_rtl_osr  = mli_osr + (mlic_w * mlic_rtl_num),
                mlic_rtl_osl  = jQuery(window).width() - (mlic_rtl_osr);
            if(mlic_rtl_osl < 0){
                jQuery(this).removeClass('rtl');
            }
        });

    });
}

jQuery(window).on('resize',function(){
    sj_area_space();
    mmenu_sub_direction(mmenu_ul,mmenu_li,sfmmenu_ul);
});

function mainCollapse(){
      /*--- Collaspe ---*/

      var kopa_collapse = jQuery('.kopa-collapse');
      if(kopa_collapse.length){
          kopa_collapse.addClass('kopa-collapse-in');
      }
      
      /* accordion */
      var kopa_accordion = jQuery('.kopa-accordion');
      if(kopa_accordion.length){
          kopa_accordion.each(function(){
  
              var panel = jQuery(this).find('.collapse-panel');
  
              panel.each(function(){
                  var panel_heading   = jQuery(this).find('.panel-heading'),
                      panel_content   = jQuery(this).find('.panel-content');
  
                  
                  if (jQuery(this).hasClass('active')) {
                      jQuery(this).find('.panel-content').css('display','block');
                  }
  
                  panel_heading.on('click', function(e){
                      e.preventDefault();
                      if (!jQuery(this).closest(panel).hasClass('active')) {
                          jQuery(this).closest(kopa_accordion).find('.active').removeClass('active');
                          jQuery(this).closest(kopa_accordion).find('.panel-content').slideUp();
                          jQuery(this).closest(panel).addClass('active');
                          jQuery(this).closest(panel).find(panel_content).slideDown();
                      }
                  });
              });
  
          });
      }
  
    //   /* toggle */
    //   var kopa_toggle = jQuery('.kopa-toggle');
    //   if(kopa_toggle.length){
    //       kopa_toggle.each(function(){
  
    //           var panel = jQuery(this).find('.collapse-panel');
  
    //           panel.each(function(){
    //               var panel_heading   = jQuery(this).find('.panel-heading'),
    //                   panel_content   = jQuery(this).find('.panel-content');
  
    //               if (jQuery(this).hasClass('active')) {
    //                   jQuery(this).find('.panel-content').css('display','block');
    //               }
  
    //               panel_heading.on('click', function(e){
    //                   e.preventDefault();
    //                   if (jQuery(this).closest(panel).hasClass('active')) {
    //                       jQuery(this).closest(panel).removeClass('active');
    //                       jQuery(this).closest(panel).find(panel_content).slideUp();
    //                   }
    //                   else{
    //                       jQuery(this).closest(panel).addClass('active');
    //                       jQuery(this).closest(panel).find(panel_content).slideDown();
    //                   }
    //               });
    //           });
  
    //       });
    //   }
}





jQuery(document).ready(function() {

/**
 * UI
 * -------------------------------------------------------------------
 */

	/*--- select ---*/

 	var kopa_select = jQuery(".kopa-select");
	if(kopa_select.length){
	    kopa_select.each(function(){
	    	jQuery(this).find('select').selectmenu({
	    		appendTo: ".kopa-select-list",
	    		disabled: false
	    	});
	    });
	}

	/*--- datepicker ---*/

    var kopa_datepicker = jQuery(".kopa-datepicker");
    if(kopa_datepicker.length){
        kopa_datepicker.each(function(){
            jQuery(this).find('.kopa-datepicker-input').datepicker({
            	showOn: "button",
            	buttonText: ""
            });
        });
    }

    /*--- slider-filter ---*/

    var kopa_slider_filter = jQuery('.kopa-slider-filter');
    if(kopa_slider_filter.length){
        kopa_slider_filter.each(function(){
            var uislider = jQuery(this).find('.slider-range'),
                uimin    = jQuery(this).find('.from'),
                uimax    = jQuery(this).find('.to');
            uislider.slider({
                range: true,
                min: 0,
                max: 96,
                values: [ 9, 69 ],
                slide: function( event, ui ) {
                    uimin.text("$" + ui.values[ 0 ]);
                    uimax.text("$" + ui.values[ 1 ]);
                }
            });
            uimin.text( "$" + uislider.slider( "values", 0 ));
            uimax.text( "$" + uislider.slider( "values", 1 ));
        });
    }


/**
 * Basic Structure
 * -------------------------------------------------------------------
 */

    /*--- radio-box ---*/

    var i1 = 0;
    if(jQuery('.widget_rating_filter').length){    
        jQuery('.widget_rating_filter').each(function() {
            var kopa_radio_box = jQuery(this).find('.kopa-radio-box');
            kopa_radio_box.each(function() {
                var item_index_1 = 'rb' + i1 + jQuery(this).index();
                jQuery(this).find('input').attr("id",item_index_1);
                jQuery(this).find('label').attr("for",item_index_1);
            }); 
            ++i1;
        }); 
    }
    
    /*--- check-box ---*/

    if(jQuery('.widget_feature_filter').length){    
        jQuery('.widget_feature_filter').each(function() {
            var kopa_check_box = jQuery(this).find('.kopa-check-box');
            kopa_check_box.each(function() {
                var item_index_1 = 'cb' + i1 + jQuery(this).index();
                jQuery(this).find('input').attr("id",item_index_1);
                jQuery(this).find('label').attr("for",item_index_1);
            });
            ++i1;
        }); 
    }

    
    /*--- input[type="file"] ---*/
    
    if(jQuery('.kopa-input-file').length){    
        jQuery('.kopa-input-file').each(function() {

            var item_index_1 = 'if' + i1 + jQuery(this).index(),
                input_file = jQuery(this).find('.input-file');

            input_file.find('input').attr("id",item_index_1);
            input_file.find('label').attr("for",item_index_1);

            var if_label     = input_file.find('label'),
                if_label_val = if_label.html(),
                if_input     = input_file.find('input');

            if_input.on('change', function(e){
                var fileName = '';
                if( if_input.files && if_input.files.length > 1 )
                    fileName = (if_input.getAttribute('data-multiple-caption')).replace('{count}', if_input.files.length);
                else if(e.target.value)
                    fileName = e.target.value.split('\\').pop();

                if(fileName)
                    if_label.find('span').html(fileName);
                else
                    if_label.html(if_label_val);
            });
            ++i1;
            
        }); 
    }

    /*--- dropdown ---*/

    var sj_ddn = jQuery('.sj-dropdown');
    if(sj_ddn.length) {
        jQuery(sj_ddn).each(function() {
            var sj_this        = jQuery(this),
                sj_ddn_btn     = sj_this.find('.sj-dropdown-btn'),
                sj_ddn_content = sj_this.find('.sj-dropdown-content');
            sj_ddn_btn.on('click', function(){
                sj_ddn_content.toggleClass('active');
            });
            jQuery("html").mouseup(function (e){
                if (!sj_this.is(e.target) && sj_this.has(e.target).length === 0){
                    sj_ddn_content.removeClass('active');
                }
            });
        });
    }

    /*--- tabs ---*/

    var kopa_tab = jQuery('.kopa-tab');
    if(kopa_tab.length){
        kopa_tab.each(function(){

            var tab_nav_item        = jQuery(this).children('.tab-navigation').find('a'),
                tab_content         = jQuery(this).children('.tab-content'),
                tab_content_item    = tab_content.children('.tab-panel'),
                tab_nav_mobile      = jQuery(this).children('.tab-navigation-mobile');

            tab_nav_item.on('click', function(e){
                e.preventDefault();
                if (!jQuery(this).hasClass('active')) {
                    var item_parent = jQuery(this).closest('li'),
                        item_index  = item_parent.index(),
                        tab_this    = jQuery(this).closest(kopa_tab);
                    tab_this.children('.tab-navigation').find('li').removeClass('active');
                    tab_this.children('.tab-content').children('.tab-panel').removeClass('active');
                    item_parent.addClass('active');
                    tab_this.find(tab_content_item).each(function(){
                        if(jQuery(this).index() == item_index){
                            jQuery(this).addClass('active');
                        }
                    });
                }
            });

            if (tab_nav_mobile.length) {
                var tab_nav_mobile_item = tab_nav_mobile.find('ul').children();
                tab_nav_mobile_item.each(function(){
                    var item_mb_index    = jQuery(this).index(),
                        item_mb_content  = jQuery(this).find('a').text();
                    jQuery(this).on('click', function(e){
                        e.preventDefault();
                        jQuery(this).parent().removeClass('active');
                        jQuery(this).parent().prev().text(item_mb_content);
                        var tab_nav_item_closest = jQuery(this).closest(tab_nav_mobile).prev('.tab-navigation').find('li');
                        tab_nav_item_closest.each(function(){
                            if(jQuery(this).index() == item_mb_index){
                                jQuery(this).find('a').trigger('click');
                            }
                        });
                    });
                });
            };

        });
    }


  

    /*--- layout-options ---*/

    var kopa_looption = jQuery('.kopalisting-layout-options');
    if(kopa_looption.length){
        kopa_looption.each(function(){
	        var kopa_looption_item = kopa_looption.find('li');
	        kopa_looption_item.on('click', function(){
	            jQuery(this).closest(kopa_looption).find('li').removeClass('active');
	            jQuery(this).addClass('active');
	        });
	        jQuery('.list-view').on('click', function(){
		        jQuery(this).closest('.kopalisting-item-list-header').next().addClass('list-layout');
		        mHeight();
		    });

		    jQuery('.grid-view').on('click', function(){
		        jQuery(this).closest('.kopalisting-item-list-header').next().removeClass('list-layout');
		        mHeight();
		    });
        });
    }



/**
 * Owl Carousel
 * -------------------------------------------------------------------
 */

    /*--- Sync ---*/

    var single_item_gallery = jQuery(".kopalisting-item").find('.item-tab-gallery');
    if(single_item_gallery.length){
        single_item_gallery.each(function(){
            var sync1 = jQuery(this).find('.sync1');
            var sync2 = jQuery(this).find('.sync2');

            function syncPosition(el){
                var current = this.currentItem;
                sync2.find(".owl-item").removeClass("synced").eq(current).addClass("synced");
                // if(sync2.data("owlCarousel") !== undefined){
                //     center(current)
                // }
            }

            function center(number){
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for(var i in sync2visible){
                  if(num === sync2visible[i]){
                    var found = true;
                  }
                }
             
                if(found===false){
                    if (undefined != sync2visible){
                        if(num > sync2visible[sync2visible.length-1]){
                            sync2.trigger("owl.goTo", num - sync2visible.length+2)
                        }else{
                            if(num - 1 === -1){
                                num = 0;
                            }
                            sync2.trigger("owl.goTo", num);
                        } 
                    }
                } else if(num === sync2visible[sync2visible.length-1]){
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if(num === sync2visible[0]){
                    sync2.trigger("owl.goTo", num-1)
                }  
            }

            sync1.each(function() {
                var item       = jQuery(this).data('item');
                var auto       = jQuery(this).data('auto');
                var navigation = jQuery(this).data('navigation');
                var pagination = jQuery(this).data('pagination');

                jQuery(this).owlCarousel({
                    singleItem: true,
                    slideSpeed: 1000,
                    autoPlay: auto,
                    navigation: navigation,
                    pagination: pagination,
                    navigationText: false,
                    afterAction: syncPosition,
                    responsiveRefreshRate: 200
                });
            });

            sync2.each(function() {
                var item         = jQuery(this).data('item');
                var itemsDesktop = jQuery(this).data('item-desktop');
                var itemsTablet  = jQuery(this).data('item-tablet');
                var itemsMobile  = jQuery(this).data('item-mobile');
                var auto         = jQuery(this).data('auto');
                var navigation   = jQuery(this).data('navigation');
                var pagination   = jQuery(this).data('pagination');

                jQuery(this).owlCarousel({
                    items: item,
                    itemsDesktop: itemsDesktop,
                    itemsTablet: itemsTablet,
                    itemsTabletSmall: itemsMobile,
                    slideSpeed: 1000,
                    autoPlay: auto,
                    navigation: navigation,
                    pagination: pagination,
                    navigationText: false,
                    afterAction: syncPosition,
                    responsiveRefreshRate: 100,
                    afterInit: function(el) {
                        el.find(".owl-item").eq(0).addClass("synced");
                    }
                });

                // var sync2_w    = sync2.width() + 20,
                //     sync2_item = sync2.find('.owl-item'),
                //     sync2_item_w = sync2_w/item;
                // sync2_item.css('max-width',sync2_item_w);

            });

            sync2.on("click", ".owl-item", function(e) {
                e.preventDefault();
                var number = jQuery(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });  

        });
    }


/*_____________ google maps _____________*/

    if (jQuery('.item-tab-map').length > 0) {

        var id_map = jQuery('.item-tab-map').attr('id');
        var lat = parseFloat(jQuery('.item-tab-map').attr('data-latitude'));
        var lng = parseFloat(jQuery('.item-tab-map').attr('data-longitude'));
        var place = jQuery('.item-tab-map').attr('data-place');
        var height = jQuery('.item-tab-map').attr('data-height');
        var iconBase = '';


        var map = new GMaps({
            el: '#'+id_map,
            lat: lat,
            lng: lng,
            zoom: 10,
            zoomControl : true,
            zoomControlOpt: {
              style : 'SMALL',
              position: 'TOP_RIGHT'
            },
            panControl : false,
            streetViewControl : false,
            mapTypeControl: false,
            overviewMapControl: false,
            scrollwheel: false,
            styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#e9e4e1"},{"visibility":"on"}]},{"featureType":"landscape","elementType":"labels.text","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"labels.text.fill","stylers":[{"color":"#797979"}]},{"featureType":"landscape","elementType":"labels.text.stroke","stylers":[{"color":"#e9e4e1"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#d8cdcd"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway.controlled_access","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway.controlled_access","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit.station","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#f6f5f1"},{"visibility":"on"}]}]
        });
        map.addMarker({
            lat: lat,
            lng: lng,
            title: place,
            icon: iconBase 
        });

        jQuery('.item-tab-map').css({
            'height': height,
            'width': '100%'
        });

    }


/*_____________ widget _____________*/


    /* widget_categories_filter */
    var wg_cat_filter = jQuery('.widget_categories_filter');
    if(wg_cat_filter.length){
        wg_cat_filter.each(function(){
            var wgcf_menu    = jQuery(this).find('.menu'),
                wgcf_item    = wgcf_menu.children('li');
               
            wgcf_item.has('ul').addClass('wu').prepend('<i></i>');

            var wgcf_item_hc = wgcf_menu.children('.wu');

            wgcf_item_hc.each(function(){
                var wgcf_item_btn = jQuery(this).children('i');
                wgcf_item_btn.on('click', function(e){
                    e.preventDefault();
                    jQuery(this).closest(wgcf_item_hc).toggleClass('active');
                    jQuery(this).closest(wgcf_item_hc).children('ul').slideToggle();
                });
            });
                
        });
    }



/**
 * matchHeight
 * -------------------------------------------------------------------
 */

     if(jQuery('.kopa-mh').length){
        mHeight();
    }


});


function mHeight(){
    jQuery('.kopa-mh').children().matchHeight();
}
import { Observable } from 'rxjs/Observable';
import { Auth } from './../../shared/services/auth.service';
import { ProductosService, Imagen, Categoria, Ubicacion, Ciudad, CiudadesService, Producto, CategoriasService } from './../../shared/services/app.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'app-admin-producto',
  templateUrl: './admin-producto.component.html',
  styleUrls: ['./admin-producto.component.css']
})
export class AdminProductoComponent implements OnInit {
  _producto: any;
  _errorBackEnd: string = '';
  _categorias: Categoria[] = [];
  _ciudades: Ciudad[] = [];
  _images: Array<any> = [];
  _saved: boolean;

  // Form Components
  formProducto: FormGroup;
  fb: FormBuilder = new FormBuilder();

  // Form Fields
  _titulo: string = '';
  _precio: number = 0.0;
  _descripcion: string = '';
  _emailContacto: string = '';
  _imagenes: Imagen[] = [];
  _categoria: Categoria = { id: 0, nombre: '' };
  _idCategoria: number = 0;
  _ubucacion: Ubicacion = { id: 0, idCiudad: 0, latitud: 0, longitud: 0 };
  _pausado: boolean = false;
  _idUsuario: number = 0;
  _id: number = 0;
  _productoSaved: any;



  // Form Validations
  formErrors = {
    'Titulo': '',
    'Precio': '',
    'Descripcion': '',
    'EmailContacto': '',
    'Imagenes': '',
    'Categoria': '',
    'Ubicacion': '',
    'Pausado': ''
  };
  validationMessages = {
    'Titulo': {
      'required': 'Debe ingresar un título'
    },
    'Precio': {
      'required': 'Debe ingresar un precio'
    },
    'Descripcion': {
      'required': 'Debe ingresar una descripción'
    },
    'EmailContacto': {
      'required': 'Debe ingresar un mail de contacto',
      'email': 'El formato del mail es incorrecto'
    },
    'Imagenes': {
      'required': ''
    },
    'Categoria': {
      'required': 'Por favor, seleccione una categoría'
    },
    'Ubicacion': {
      'required': ''
    },
    'Pausado': {
      'required': ''
    }
  };


  constructor(
    private _route: ActivatedRoute,
    private _productosService: ProductosService,
    private _ciudadesService: CiudadesService,
    private _categoriasService: CategoriasService,
    private _auth: Auth,
    private _router: Router
  ) { }

  ngOnInit() {




    this._route.params.subscribe((x: Event) => {

      this._categoriasService.getAll().flatMap((cats) => {
        this._categorias = cats;

        return this._ciudadesService.getAll();

      }).flatMap((ciudades) => {
        this._ciudades = ciudades;

        if (x['id']) {
          return this._productosService.get(+x['id']);
        } else {
          return Observable.of(null);
        }

      }).subscribe(p => {

        if (p) {
          this._producto = p;
          this._id = +x['id'];
        }

        this.buildForm();
      });


    })
  }


  buildForm() {
    this.formProducto = this.fb.group({
      'Titulo': [this._titulo, [Validators.required]],
      'Precio': [this._precio, [Validators.required]],
      'Descripcion': [this._descripcion, [Validators.required]],
      'EmailContacto': [this._emailContacto, [Validators.required, Validators.email]],
      // 'Imagenes': [this._imagenes, [Validators.required]],
      'Categoria': [this._imagenes, [Validators.required]],
      'Ubicacion': [this._imagenes, [Validators.required]],
      'Pausado': [this._imagenes, [Validators.required]],
    });

    this.formProducto.valueChanges
      .subscribe(data => this.onValueChanged(data, this.formProducto, this.formErrors, this.validationMessages));

    this.onValueChanged(); // (re)set validation messages now);



    this.formProducto.setValue({
      'Titulo': (this._producto == null) ? '' : this._producto.titulo,
      'Precio': (this._producto == null) ? '' : this._producto.precio,
      'Descripcion': (this._producto == null) ? '' : this._producto.descripcion,
      'EmailContacto': (this._producto == null) ? '' : this._producto.emailContacto,
      'Categoria': (this._producto == null) ? this._categorias[0].id : this._producto.idCategoria,
      'Ubicacion': (this._producto == null) ? this._ciudades[0].id : this._producto.ubicacion.idCiudad,
      'Pausado': (this._producto == null) ? false : this._producto.pausado,
    });


    if (this._id !== 0) {

      for (var i in this._producto.imagenes) {
        var name = this._producto.imagenes[i].url.split('/')[this._producto.imagenes[i].url.split('/').length - 1];

        let img = { id: this._producto.imagenes[i].id, name: name };
        this._images.push(img);

      }

    }



    // if (this._edit !== 0) {
    //   this.selectItem(this.formProducto, this._user);
    // } else {
    //   let user: any = {
    //     AccountID: this._edit,
    //     Firstname: '',
    //     Lastname: '',
    //     EMail: '',
    //     LanguageCode: this._languages[0]['LanguageCode']
    //   };

    //   this._user = user;
    //   this.selectItem(this.formProducto, this._user);
    // }
  }


  onSave() {
    if (this.formProducto.invalid) {
      return;
    }


    var categoria: any = {};
    for (var i in this._categorias) {
      if (this._categorias[i].id === this.formProducto.controls['Categoria'].value) {
        categoria = { id: this._categorias[i].id, nombre: this._categorias[i].nombre };
      }
    }

    let ciudad: any = {};
    for (var i in this._ciudades) {
      if (this._ciudades[i].id === this.formProducto.controls['Ubicacion'].value) {
        ciudad = { id: this._ciudades[i].id, nombre: this._ciudades[i].nombre, ubicaciones: null };
      }
    }


    console.log(this.formProducto.controls['Ubicacion'].value);

    let producto: Producto = {
      id: this._id,
      descripcion: this.formProducto.controls['Descripcion'].value,
      titulo: this.formProducto.controls['Titulo'].value,
      precio: this.formProducto.controls['Precio'].value,
      idCategoria: this.formProducto.controls['Categoria'].value,
      pausado: this.formProducto.controls['Pausado'].value,
      imagenes: [],
      ubicacion: { id: 0, idCiudad: this.formProducto.controls['Ubicacion'].value, latitud: 0, longitud: 0 },
      idUsuario: this._auth.showProfile().sub,
      emailContacto: this._auth.showProfile().email
    };

    var _newImages = [];
    for (var i in this._images) {
      if (this._images[i] instanceof File) {
        _newImages.push(this._images[i]);
      }
    }


    this._productosService.createProducto(producto, _newImages, (success) => {
      this._saved = true;
      this._productoSaved = success;
      console.log(success);
    }, (error) => {
      console.log(error);

    })

  }

  onAddImage(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];

      let img = { id: 0, idProducto: this._id, url: file.name };

      if (this._images.indexOf(img) < 0) {
        //Lógica para subir la imagen

        this._images.push(file);
      }



      // var img = document.querySelector("#preview img");
      // img.file = file;

      // var reader = new FileReader();
      // reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
      // reader.readAsDataURL(file);
    }
  }

  generateArray(obj) {

    if (obj == null) {
      return;
    }

    return Object.keys(obj).map((key) => { return obj[key] });
  }

  onRemoveImage(image) {
    this._images.splice(this._images.indexOf(image), 1);
  }

  onValueChanged(data?: any, form?: any, formErrors?: any, validationMessages?: any) {

    if (!form) {
      return;
    }
    // if (formErrors) {
    // 	formErrors.forEach(field => {
    // 		formErrors[field] = '';
    // 		const control = form.get(field);
    // 		if (control && control.dirty && !control.valid) {
    // 			const messages = validationMessages[field];
    // 			control.errors.forEach(key => {
    // 				formErrors[field] += messages[key] + ' ';
    // 			});
    // 		}
    // 	});
    // }
    for (const field in formErrors) {
      // clear previous error message (if any)
      formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = validationMessages[field];
        for (const key in control.errors) {
          formErrors[field] += messages[key] + ' ';
        }
      }
    }
    if (!this.formProducto.invalid) {
      this._errorBackEnd = '';
    }
  }
  goToProduct() {
    this._productoSaved = JSON.parse(this._productoSaved);
    this._router.navigate(['producto', this._productoSaved.id]);
  }
}

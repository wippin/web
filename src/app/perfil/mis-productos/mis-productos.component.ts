import { Producto, ProductosService } from '../../shared/services/app.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-mis-productos',
    templateUrl: './mis-productos.component.html',
    styleUrls: [`./mis-productos.css`]
})
export class MisProductosComponent implements OnInit {
    _listaProductos: Producto[];
    _fullResponse: Array<any> = [];
    constructor(private _productosService: ProductosService) {


    }
    ngOnInit(): void {

        this._productosService.getPaginated(false, 1, 100).subscribe(data => {
            console.log(data);

            this._listaProductos = data.results;

        });
        // this._listaProductos = this._productosService
    }

    remove(id) {
        this._productosService.delete(id).subscribe(() => {
            this._productosService.getPaginated(false, 1, 100).subscribe(data => {
                this._listaProductos = data.results;
            });

        })
    }

}
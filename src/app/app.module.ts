import { LoadingFullScreenComponent } from './shared/loading-full-screen/loading-full-screen.component';
import { AuthGuard } from './shared/services/auth-guard.service';
import { SharedService } from './shared/services/shared.service';
import { TopnavComponent } from './shared/topnav/topnav.component';
import { PerfilComponent } from './perfil/perfil.component';
import { AdminProductoComponent } from './perfil/admin-producto/admin-producto.component';
import { MisProductosComponent } from './perfil/mis-productos/mis-productos.component';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { OwlModule } from 'ng2-owl-carousel';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
// import { ProductosService } from './shared/services/productos.service';
// import { CategoriasService } from './shared/services/categorias.service';
import { Auth } from './shared/services/auth.service';
import { CategoriasService, TransaccionesService, ProductosService, CiudadesService } from './shared/services/app.service';
import { routing } from './app.routes';
import { ListadoProductosComponent } from './listado-productos/listado-productos.component';
import { ProductoComponent } from './producto/producto.component'
import { provideAuth, AuthHttp, AuthConfig } from 'angular2-jwt';
export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    tokenGetter: (() => localStorage.getItem('id_token'))
  }), http, options);
}

export function productoServiceFactory(http: Http) {
  return new ProductosService(http, environment.urlBase);
};

export function categoriaServiceFactory(http: Http) {
  return new CategoriasService(http, environment.urlBase);
};
export function ciudadesServiceFactory(http:Http){
  return new CiudadesService(http,environment.urlBase);
}
@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    OwlModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ListadoProductosComponent,
    ProductoComponent,
    MisProductosComponent,
    AdminProductoComponent,
    PerfilComponent,
    TopnavComponent,
    LoadingFullScreenComponent
  ],
  providers: [  Auth, AuthGuard,{
    provide: AuthHttp,
    useFactory: authHttpServiceFactory,
    deps: [Http, RequestOptions]
  },
  {
    provide: ProductosService,
    useFactory: productoServiceFactory,
    deps:[Http]
  },
  {
    provide: CategoriasService,
    useFactory: categoriaServiceFactory,
    deps:[Http]
  }
  ,
  {
    provide: CiudadesService,
    useFactory: ciudadesServiceFactory,
    deps:[Http]
  },SharedService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }

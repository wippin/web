import { Subject } from 'rxjs/Subject';
import { ProductosService } from './../shared/services/app.service';
import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked, AfterViewInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit, AfterViewChecked, AfterViewInit {
  _producto: any;
  images: Array<any> = [
    {url:'http://placehold.it/870x450'},
    {url:'http://placehold.it/870x450'},
    {url:'http://placehold.it/870x450'},
    {url:'http://placehold.it/870x450'},
    {url:'http://placehold.it/870x450'},
    {url:'http://placehold.it/870x450'},
    {url:'http://placehold.it/870x450'}
  ];

  selected: number = 0;
  options: any = {
    items: 1,
    dots: false,
    nav: true,
    slideSpeed: 600,
    autoPlay: false,
    navContainerClass: 'owl-controls bigger-nav-button',
    onChanged: ((e) => {
      this.selected = e.item.index;
    }),
    navText: false,
  };

  options2: any = {
    items: 5,
    dots: false,
    nav: true,
    autoPlay: false,
    navContainerClass: 'owl-controls',
    navText: false,
    navSpeed: 1000
  };


  @ViewChild('owlMain') owlMain: ElementRef;
  @ViewChild('owlThumb') owlThumb: ElementRef;

  constructor(
    private zone: NgZone,
    private _route: ActivatedRoute
    , private _productosService: ProductosService) { }

  ngOnInit() {

    this._route.params.subscribe(x => {
      this._productosService.get(+x.id).subscribe(x => {
        console.log(x);

        this._producto = x;
        this.images = x['imagenes'];

      });
    })
  }

  ngAfterViewChecked() {



  }

  ngAfterViewInit() {

  }

  getMyStyles() {
    let myStyles = { 'background': 'url("' + this._producto.imagenes[0].url + '") no-repeat center' };
    return myStyles;
  }

  selectThumb(i) {
    if (this.owlMain) {
      this.selected = i;
      this.owlMain['$owlChild'].trigger('to.owl.carousel', [i, 600])
    }
  }

}

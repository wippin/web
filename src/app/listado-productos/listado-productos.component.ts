import { ProductosService, FilterParams } from '../shared/services/app.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
@Component({
  selector: 'app-listado-productos',
  templateUrl: './listado-productos.component.html',
  styleUrls: ['./listado-productos.component.css']
})
export class ListadoProductosComponent implements OnInit {
  _listProductsSub: any;
  _filterParams: FilterParams;
  _idCategoria: number;
  _idUbicacion: number;
  _searchText: string;
  _sortings: Array<any> = [
    {id: 'ID', name: 'Default'},
    {id: 'AZ', name: 'A-Z'},
    {id: 'ZA', name: 'Z-A'},
  ];
  _sorting: string = 'ID';
  

  constructor(
    private _productosService: ProductosService
    , private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this._route.params.subscribe((x: any) => {

      this._idCategoria = x.idCategoria;
      this._idUbicacion = x.idUbicacion;
      this._searchText = x.searchText;
      this._filterParams =
        {
          idCategoria: this._idCategoria,
          ubicacion: null,
          texto: this._searchText,
          ascendant: null

        }
      this._productosService.getFilteredBy(this._filterParams).subscribe((data) => {

        this._listProductsSub = data;
      });
    });
  }


  onChange(val) {
    
    let asc = null;
    this._sorting = val.target.value;
    switch(val.target.value){
      case 'ID':
        asc = null;
        break;
      case 'AZ':
        asc = true;
        break;
      case 'ZA':
        asc = false;
        break;
    }

    this._filterParams =
      {
        idCategoria: this._idCategoria,
        ubicacion: null,
        texto: this._searchText,
        ascendant: asc

      }
    this._productosService.getFilteredBy(this._filterParams).subscribe((data) => {
      console.log(data);
      
      this._listProductsSub = data;
    });

  }

}

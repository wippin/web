import { MisProductosComponent } from './perfil/mis-productos/mis-productos.component';
import { PerfilComponent } from './perfil/perfil.component';
import { AdminProductoComponent } from './perfil/admin-producto/admin-producto.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductoComponent } from './producto/producto.component';
import { ListadoProductosComponent } from './listado-productos/listado-productos.component';
import { AuthGuard } from './shared/services/auth-guard.service';
// import { AuthGuard } from './shared/services/auth-guard.service';
export const routing = RouterModule.forRoot([

    {
        path: 'search/:idCategoria',
        component: ListadoProductosComponent
    },
    {
        path: 'search/:idCategoria/:idUbicacion',
        component: ListadoProductosComponent
    },
    {
        path: 'search/:idCategoria/:idUbicacion/:searchText',
        component: ListadoProductosComponent
    },
    {
        path: '', component: HomeComponent,
    },
    {
        path: 'producto/:id', component: ProductoComponent
    },
    {
        path: 'admin-producto',
        component: AdminProductoComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'admin-producto/:id',
        component: AdminProductoComponent
    },
    {
        path: 'mis-productos',
        component: MisProductosComponent
    }



]);
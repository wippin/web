import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  scrollToTop() {
    window.scrollTo(0, 0);
  }
  getDisplayScroll() {
    if (window.scrollY > 200) {
      return 'block';
    }
    else{
      return 'none';
    }
  }
}

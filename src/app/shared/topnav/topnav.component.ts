import { SharedService } from './../services/shared.service';
import { AuthHttp } from 'angular2-jwt';
import { Producto, CiudadesService } from '../services/app.service';
import { Auth } from '../services/auth.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ProductosService, CategoriasService } from '../services/app.service';
@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.css']
})
export class TopnavComponent implements OnInit {
  _masVistosSub;
  _listaCategoriasSub;
  _searchText: string = '';
  _selectedCategoria: any;
  _selectedUbicacion: any;
  _file: any;


  constructor(
    private _router: Router
    , public _sharedService:SharedService
    , public _auth: Auth
    , private _authHttp: AuthHttp
    , private _el: ElementRef) { }

  ngOnInit() {
  }

  changeListner(event: any) {
  }

  showProfile() {
    
    console.log(this._auth.showProfile());
  }

  onSearchButtonClick() {
    this._sharedService.onSearchClick.next();
    this._sharedService.MENU_ACTIVE=false;
    // this._router.navigate(['search/' + 0 + '/' + 0 + '/' + ((this._searchText) ? this._searchText : null)])
  }


}

//"assets/css/owl.carousel.css"
  // "assets/js/jquery.mCustomScrollbar.concat.min.js",
        // "assets/js/jquery.mousewheel.min.js",
        // "assets/js/jquery.validate.min.js",
        // "assets/js/owl.carousel.min.js",
        // "assets/js/jquery.magnific-popup.min.js",
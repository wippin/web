import { Component, } from '@angular/core';

@Component({
    selector: 'loading-full-screen',
    template: `   <div class="circle"></div>
    <div class="overlay"></div>`,
    styles: [`
    .circle {
  box-sizing: border-box;
  width: 5em;
  height: 5em;
  border-radius: 100%;
  border: 10px solid rgba(255, 255, 255, 0.2);
  border-top-color: #FFF;
  animation: spin 1s infinite linear;
  position: fixed;
  top:0;
  left:0;
  bottom: 0;
  right: 0;
  z-index: 10001;
  margin: auto;
  overflow: hide;
}

.overlay {
  content: '';
  display: block;
  position: fixed;
  margin:auto;
  z-index: 10000;
  top: 0;
  left: 0;
  width: 150vw;
  height: 150vh;
  background-color: rgba(44, 62, 80, 0.2);
}


@keyframes spin { 
  100% { 
    transform: rotate(360deg); 
  } 
} 

    `]
})

export class LoadingFullScreenComponent {

}

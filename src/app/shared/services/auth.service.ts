import { Router } from '@angular/router';
import { SharedService } from './shared.service';
import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import Auth0Lock from "auth0-lock";

const PROFILE = 'profile';
const ID_TOKEN = 'id_token';
@Injectable()
export class Auth {

    userProfile: any;
    options: Auth0LockConstructorOptions = {
        auth: {
            audience: 'https://api.wippin.com',
            params: {
                scope: 'openid profile email'
            }
        },
        oidcConformant: true,
        language: 'es',
        theme: {
            logo: 'assets/images/header/logo.png',
            primaryColor: '#86dca4',
        }

    };
    lock = new Auth0Lock('7DdH4VkEuXsIFb5sLY0VySCWed9yVNw4', 'codenmate.auth0.com', this.options);
    constructor(private _sharedService: SharedService,
        private _router: Router) {
        this.userProfile = JSON.parse(localStorage.getItem(PROFILE));
        this.lock.on('authenticated', authResult => {
            if (this._sharedService.getPreviousRoute() !== undefined && this._sharedService.getPreviousRoute() !== '') {
                this._router.navigate([this._sharedService.getPreviousRoute()]);
                this._sharedService.setPreviousRoute('');
            }

            localStorage.setItem(ID_TOKEN, authResult.accessToken);
            this.lock.getUserInfo(authResult.accessToken, (error, profile) => {
                if (error) {
                    console.log('Error', error);
                    return;
                }
                localStorage.setItem(PROFILE, JSON.stringify(profile));
            })
        })
    }

    public login() {
        this.lock.show();
    }
    public isAuthenticated() {
        return tokenNotExpired(ID_TOKEN);
    }
    public showProfile() {
        return JSON.parse(localStorage.getItem(PROFILE));
    }
    public getBearerToken() {
        return localStorage.getItem(ID_TOKEN);
    }


    public logout() {
        localStorage.removeItem(ID_TOKEN);
        localStorage.removeItem(PROFILE);
        this.userProfile = null;
        this._router.navigate([''])
    }
}
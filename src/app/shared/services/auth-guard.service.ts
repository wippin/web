import { Auth } from './auth.service';
import { SharedService } from './shared.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import * as Rx from "rxjs/Rx";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private _router: Router,
        private _sharedService: SharedService,
        private _auth: Auth
    ) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        this._sharedService.setPreviousRoute(state.url);
        let observable = Rx.Observable.create(observer => {
            if (this._auth.isAuthenticated()) {
                observer.next(true);
            } else {
                this._auth.login();
                observer.next(false);
            }
        })
        return observable;
    }


}
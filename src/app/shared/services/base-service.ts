import { environment } from './../../../environments/environment';
import { Producto } from './app.service';
// import * as generated from "./serviceClients";
import { RequestOptionsArgs } from '@angular/http'; // ignore

export class BaseService {
    transformOptions(options: RequestOptionsArgs) {

        options.headers.append("Authorization", "Bearer " + localStorage.getItem('id_token'));

        return Promise.resolve(options);
    }

    createProducto(producto: Producto, files: File[], successProc: (content: Object) => void, errorProc: (exception: any) => void) {
        if (files === null) {
            if (files === undefined) {
                return;
            }
        }

        let xhr: XMLHttpRequest = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    successProc(xhr.responseText);
                } else {
                    try {
                        errorProc(xhr.responseText);
                    } catch (exception) {
                        errorProc(exception);
                    }

                }
            }
        };

        xhr.open('POST', environment.urlBase + 'api/productos', true);

        let formData = new FormData();

        
         xhr.setRequestHeader('Access-Control-Allow-Origin','*')
        //   xhr.setRequestHeader("Content-Type", "multipart/form-data");
        formData.append('producto', JSON.stringify(producto));
        if (files !== null && files !== undefined) {
            files.map(f => {
                formData.append('file[]', f);
            })
        }
        xhr.send(formData);
        // });

    }
}
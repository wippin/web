import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class SharedService {
    MENU_ACTIVE: boolean = false;
    public onSearchClick: Subject<boolean> = new Subject<boolean>();
    // PREVIOUS_ROUTE: any;
    constructor() { }

    getPreviousRoute() {
        let previousRoute = localStorage.getItem('previous_route');
        if (previousRoute === undefined) {
            return '';
        }
        return previousRoute;
    }

    setPreviousRoute(route) {
        localStorage.setItem('previous_route', route);
    }
}
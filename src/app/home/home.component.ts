import { SharedService } from './../shared/services/shared.service';
import { AuthHttp } from 'angular2-jwt';
import { Producto, CiudadesService } from './../shared/services/app.service';
import { Auth } from './../shared/services/auth.service';
import {
  Component, OnInit, ElementRef, trigger
  , state
  , style
  , transition
  , animate,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { ProductosService, CategoriasService } from '../shared/services/app.service';

declare var mainSliderHome: any;
declare var mainCollapse: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('searchBoxState', [
      state('normal',
        style({ height: '0px' })),
      state('false',
        style({ height: '*' })),
      transition('normal => bigger, bigger=> normal', animate(200))
    ])
  ]
})
export class HomeComponent implements OnInit {
  @ViewChild('search') _search:any;
  _masVistosSub;
  _scale: boolean = false;
  _listaCategoriasSub;
  _searchText: string = '';
  _selectedCategoria: any;
  _selectedUbicacion: any;
  _selectedCiudad: any;
  _listaCiudades: any;
  _file: any;
  _listCollapsable = [{
    title: 'Fácil de coordinar',
    subtitle: 'Alquilá objetos de manera sencilla y flexible',
    content: 'Podrás coordinar el horario y el punto de encuentro para la devolución del producto.',
    active: true
  },
  {
    title: '100% Seguro',
    subtitle: '¿Alquilás con Wippin? Estas cubierto',
    content: 'Wippin es un sistema totalmente seguro y cubierto ante pérdidas o robos de cualquiera de tus productos publicados o alquilados.',
    active: false
  },
  {
    title: 'Viajá Liviano',
    subtitle: 'No cargues equipo demás',
    content: 'Wippin te permite disfrutar de tu viaje, tus aventuras y tus recorridos sin llevar equipaje extra en tu valija.',
    active: false
  },
  {
    title: 'Volvé a darle uso a tus objetos',
    subtitle: 'Si no lo usas, alquilalo! ',
    content: 'Dentro de Wippin vas a poder alquilar todos esos equipos que usas en pocas ocasiones y que quedan guardados dentro del placard. Volvé a darles un uso y ganá dinero a la vez!',
    active: false
  },
  {
    title: 'Sé parte de una Comunidad',
    subtitle: 'Entre todos nos podemos ayudar',
    content: 'Utilizando esta herramienta haces mucho más que alquilar productos. Estás siendo parte de una comunidad que no te dejará solo en tus viajes!',
    active: false
  }];

  constructor(
    private _productosService: ProductosService
    , private _categoriasService: CategoriasService
    , private _ciudadesService: CiudadesService
    , private _router: Router
    , private _sharedService: SharedService
    , public _auth: Auth
    , private _authHttp: AuthHttp
    , private _el: ElementRef) { }

  ngOnInit() {
    mainSliderHome();
    mainCollapse();
    this._masVistosSub = this._productosService.getMostViewed();

    this._listaCategoriasSub = this._categoriasService.getAll();

    this._listaCiudades = this._ciudadesService.getAll();
    this._sharedService.onSearchClick.subscribe(() => {
      this.onScale();
      this._search.nativeElement.focus();
    })
  }

  changeListner(event: any) {
    var reader = new FileReader();
    var image = this._el.nativeElement.querySelector('.image');

    reader.onload = function (e: any) {
      var src = e.target.result;
      image.src = src;
    };

    reader.readAsDataURL(event.target.files[0]);
    this._file = event.target.files[0];

  }

  showProfile() {
    console.log(this._auth.showProfile());
  }

  onSearchButtonClick() {
    this._router.navigate(['search/' , ((this._selectedCategoria) ? this._selectedCategoria.id : '') ,((this._selectedUbicacion) ? this._selectedUbicacion.ciudadId : '') ,((this._searchText) ? this._searchText : '')])
  }
  onChangeCategoria($event: any) {
    this._selectedCategoria = $event;
  }

  onChangeUbicacion($event: any) {
    this._selectedUbicacion = $event;
  }

  onChangeCiudad($event) {
    this._selectedCiudad = $event;
  }
  onCollapsableItemClick(item) {
    this._listCollapsable.map(l => {
      l.active = false;
    })
    item.active = true;
  }

  onScale() {
    this._scale = true;
    setTimeout(() => {
      this._scale = false;
    }, 400);
  }

}

import { WippinWebPage } from './app.po';

describe('wippin-web App', () => {
  let page: WippinWebPage;

  beforeEach(() => {
    page = new WippinWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
